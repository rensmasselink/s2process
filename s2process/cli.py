"""Scripts to download S2 imagery, preprocess it using sen2cor and detect
clouds using machine learning algorithm from sentinelhub"""

"""Steps
1. Query S2 data
    a.location
    b.time
    c.Max cloud (optional)?
    
2. Download data locally

3. Cloud detection using machine learning

4. Atmospheric correction using sen2cor

5. Upload to cloud storage?"""


import argparse
import datetime
import logging
import math
import os
import sys
import tempfile
import rasterio
from rasterio import windows
from rasterio import transform
import numpy as np
import shapely.wkt
import geopandas as gpd
import pandas as pd
import utm
from math import ceil
from shapely.ops import unary_union
from shapely.geometry import shape, mapping, Polygon
from sentinelhub import WmsRequest,WcsRequest, MimeType, CRS, BBox, \
    CustomUrlParam
from sentinelhub.geo_utils import to_wgs84, to_utm_bbox, wgs84_to_utm, \
    get_utm_crs, transform_bbox

from sentinelhub.data_request import WmsRequest
from sentinelhub.constants import MimeType, CustomUrlParam
from sentinelhub.common import BBox, CRS
from sentinelhub.opensearch import get_area_dates

from s2cloudless import S2PixelCloudDetector
from s2cloudless import CloudMaskRequest

# s2cloudless.test_sentinelhub_cloud_detector()
# from s2process import settings
INSTANCE_ID= '7f364c7f-6ce8-4dad-8c0b-55d3f9de2b7f'

WORKSPACE=''
BASE_PIXEL_SIZE = 10
WINDOW_SIZE=512
M_IN_DEGREE_WGS84 = float(111320)  #m at the equator

CLOUD_URLS = {1:"https://github.com/sentinel-hub/custom-scripts/blob/master"
          "/sentinel-2/cby_cloud_detection/script.js",
        2: "https://github.com/sentinel-hub/custom-scripts/blob/master" \
             "/sentinel-2/hollstein/script.js"}

PARSER = argparse.ArgumentParser(
    usage="""
        \nPGet S2 data, preprocess and cloud detection
    """
)

PARSER.add_argument(
    '--name',
    type = str,
    help='''
        directory name, which will be placed within the workspace. Needs to be
        provided!
    '''
)
PARSER.add_argument(
    '--bands',
    type = str,
    default='ALL-BANDS',
    help='''
        Provide a list of bands you want to have. If you want all bands, 
        leave blank
    '''
)
PARSER.add_argument(
    '--wkt',
    type = str,
    help='''
        AOI wkt in WGS84. The script will take the bounding box of the 
        provided polygon. 
    '''
)
PARSER.add_argument(
    '--start',
    type = str,
    help='''
        start time in format YYYY-MM-DD 
    '''
)
PARSER.add_argument(
    '--end',
    type = str,
    help='''
        end time in format YYYY-MM-DD 
    '''
)
PARSER.add_argument(
    '--resample',
    type = int,
    default=1,
    choices=[1,2,4,8,16,32,64,128,256,512],
    help='''
        Resampling. Defaults to 1 (no resampling = ~10 meter)
    '''
)

PARSER.add_argument(
    '--maxcloud',
    type = float,
    default=1,
    help='''
        Max Cloud cover per image between 0 and 1. Default 1
    '''
)
PARSER.add_argument(
    '--cloudremove',
    type = int,
    default=1,
    choices=[1,2,3],
    help='''
        Choose wich cloud detection algorithm to use. Not implemented yet
    '''
)

PARSER.add_argument(
    '--cloudthresh',
    type = float,
    default=0.5,
    help='''
        Probability threshold for removing of clouds (1: no clouds are removed,
        0: everything is removed)
    '''
)
PARSER.add_argument(
    '--separate',
    type = bool,
    default=True,
    help='''
        Whether to output separate files or make an aggregate. Default True
    '''
)

def round_coordinates(poly):
    # convert to GeoJSON format
    geojson = mapping(poly)
    geojson['coordinates'] = np.round(np.array(geojson['coordinates']), 2)
    return shape(geojson)

def get_bbox_from_wkt(wkt):
    orig_shape = shapely.wkt.loads(wkt)
    rounded_shape = round_coordinates(orig_shape)
    print(rounded_shape)
    return BBox(rounded_shape.bounds, crs=CRS.WGS84)

def get_window_size(resample):
    pixel_size = (BASE_PIXEL_SIZE /M_IN_DEGREE_WGS84) * resample
    #Max 5000x5000 pixels, we make blocks of 2048 to be on the safe side
    return BASE_PIXEL_SIZE * resample, pixel_size * WINDOW_SIZE

def wcs_request(output_directory, bands, bbox, time_start, time_end,
                pixel_size, max_cloud, cloud_thresh):
    # wcs_request = WmsRequest(data_folder=output_directory,
    #                                layer='NDVI',
    #                                bbox=bbox,
    #                                time='2017-12-15',
    #                                 width=512, height=856,
    #                                maxcc=max_cloud,
    #                                image_format=MimeType.TIFF,
    #                                instance_id=INSTANCE_ID)
                                   # time_difference = datetime.timedelta(
                                   #     hours=2),
                                   # custom_url_params = {
                                   #   CustomUrlParam.ATMFILTER: 'ATMCOR',
                                   #     CustomUrlParam.EVALSCRIPTURL: url
    # CustomUrlParam.SHOWLOGO: False}
    # )
    bands_script = 'return [B01,B02,B04,B05,B08,B8A,B09,B10,B11,B12]'
    # wcs_request = WmsRequest(data_folder=output_directory,
    #                                     layer='TRUE_COLOR',
    #                                     bbox=bbox,
    #                                     width=500,
    #                                     time=(time_start, time_end),
    #                                     instance_id=INSTANCE_ID
    #                                     )
    # print(wcs_request.get_url_list())
    # all_cloud_masks = CloudMaskRequest(ogc_request=wcs_request,
    #                                    threshold=0.1)#, all_bands=True)
    print('pixels', '%sm'% pixel_size)
    # print("PIXELS", '%sm'%pixel_size)
    wcs_request = WcsRequest(data_folder=output_directory,
                  layer='ALL-BANDS',
                                   bbox=bbox,
                                   time=(time_start, time_end),
                                   resx='%sm'%pixel_size,
                                   resy='%sm'%pixel_size,
                                   image_format=MimeType.TIFF_d32f,
                                   instance_id=INSTANCE_ID)
    # print("HAHA")
    # print(wcs_request.get_url_list())
    # wcs_request.get_data(save_data=True, redownload=True)
    all_cloud_masks = CloudMaskRequest(ogc_request=wcs_request,
                                       threshold=cloud_thresh, all_bands=True)
    return all_cloud_masks

def download(wms_request):
    wcs_bands_img = wms_request.get_data(save_data=True, redownload=True)

def progress(count, total, status=''):
    """A simple progress bar for the command line
            Args:
                count (int) the count between 0 and total
                total (int) the last iteration

            Returns Nothing
    """
    count += 1
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    p_bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (p_bar, percents, '%', status))
    sys.stdout.flush()

def get_logger(output_directory):
    logging.basicConfig(level=logging.DEBUG,)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logging.captureWarnings(True)
    # # create console handler and set level to info
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    # # create formatter
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # # add formatter to ch
    ch.setFormatter(formatter)
    # # add Handlers to logger
    logging.getLogger('').addHandler(ch)
    requests_logger=logging.getLogger('requests')
    requests_logger.setLevel('CRITICAL')
    rasterio_logger = logging.getLogger('rasterio')
    rasterio_logger.setLevel('CRITICAL')
    return logger

def check_and_make_dir(dir):
    if not os.path.exists(dir):
        os.mkdir(dir)

def make_grid(gdf, height, cut=True):
    """
    Return a grid, based on the shape of *gdf* and on a *height* value (in
    units of *gdf*). If cut=False, the grid will not be intersected with *gdf*
    (i.e it makes a grid on the bounding-box of *gdf*).
    Parameters
    ----------
    gdf: GeoDataFrame
        The collection of polygons to be covered by the grid.
    height: Integer
        The dimension (will be used as height and width) of the ceils to create,
        in units of *gdf*.
    cut: Boolean, default True
        Cut the grid to fit the shape of *gdf* (ceil partially covering it will
        be truncated). If False, the returned grid will fit the bounding box
        of *gdf*.
    Returns
    -------
    grid: GeoDataFrame
        A collection of polygons.
    """

    xmin, ymin = [i.min() for i in gdf.bounds.T.values[:2]]
    xmax, ymax = [i.max() for i in gdf.bounds.T.values[2:]]
    rows = ceil((ymax-ymin) / height)
    cols = ceil((xmax-xmin) / height)

    x_left_origin = xmin
    x_right_origin = xmin + height
    y_top_origin = ymax
    y_bottom_origin = ymax - height

    res_geoms = []
    for countcols in range(cols):
        y_top = y_top_origin
        y_bottom = y_bottom_origin
        for countrows in range(rows):
            res_geoms.append((
                (x_left_origin, y_top), (x_right_origin, y_top),
                (x_right_origin, y_bottom), (x_left_origin, y_bottom)
                ))
            y_top = y_top - height
            y_bottom = y_bottom - height
        x_left_origin = x_left_origin + height
        x_right_origin = x_right_origin + height
    if cut:
        if all(gdf.eval(
            "geometry.type =='Polygon' or geometry.type =='MultiPolygon'")):
            res = gpd.GeoDataFrame(
                geometry=pd.Series(res_geoms).apply(lambda x: Polygon(x)),
                crs=gdf.crs
                ).intersection(unary_union(gdf.geometry).convex_hull)
        else:
            res = gpd.GeoDataFrame(
                geometry=pd.Series(res_geoms).apply(lambda x: Polygon(x)),
                crs=gdf.crs
                ).intersection(unary_union(gdf.geometry).convex_hull)
        res = res[res.geometry.type == 'Polygon']
        res.index = [i for i in range(len(res))]
        return gpd.GeoDataFrame(geometry=res)

    else:
        return gpd.GeoDataFrame(
            index=[i for i in range(len(res_geoms))],
            geometry=pd.Series(res_geoms).apply(lambda x: Polygon(x)),
            crs=gdf.crs
            )

def wkt_to_gdf(wktstring, window_size):
    geometry = shapely.wkt.loads(wktstring)
    gdf = gpd.GeoDataFrame(index=[0], data={'id':0}, geometry=[geometry],
                           crs="EPSG:4326")
    return make_grid(gdf, height=window_size, cut=False)

def convert_bbox_to_utm(bbox):
    #Get LL and UR and change to UTM
    left, bottom = bbox.get_lower_left()
    right, top = bbox.get_upper_right()
    # print(utm.from_latlon(top, right))
    print(utm.from_latlon(bottom, left))

    lon, lat = bbox.get_middle()
    print(lat, lon)
    utm_crs = get_utm_crs(lat, lon, source_crs=bbox.get_crs())




    # print(utm_crs)
    # return transform_bbox(bbox, utm_crs)
    # crs = CRS.get_utm_from_wgs84(top, left)
    convert = lambda lat, lon: wgs84_to_utm(lat, lon, utm_crs=utm_crs)
    bottom, left = convert(bottom, left)
    print(bottom, left)
    top, right = convert(top, right)
    return BBox(([bottom, left], [top, right]), crs=utm_crs)


def make_output_dir(args):
    out_dir = os.path.join(WORKSPACE+args.name)
    check_and_make_dir(out_dir)
    return out_dir


def write_raster(win, cloud_masks,  output_directory, args):
    valid = []
    dates = cloud_masks.get_dates()
    print(dates)
    win_str = [str(x) for x in win]
    for i, (prob, mask, data) in enumerate(cloud_masks):
        print("DATA", prob)
        print("WIN", win)
        print(np.nanmedian(prob))
        mask3d = np.dstack([prob] * data.shape[2])
        valid = np.where(mask3d>args.cloudthresh, np.nan, data)
        if args.separate:
            count = valid.shape[-1]
            date = datetime.datetime.strftime(dates[i], "%Y%m%d")
            check_and_make_dir(os.path.join(output_directory, date))
            f_name = '{}/S2_aggregate_cloudless_{}_{}.tif'.format(
                os.path.join(output_directory, date),
                date, '.'.join(win_str))
            with rasterio.open(f_name, 'w',
                               **{'width': valid.shape[1],
                                  'height': valid.shape[0],
                                  'dtype': rasterio.dtypes.float32,
                                  'count': count,
                                  'crs': 'EPSG:4236',
                                  'driver': 'GTiff',
                                  'nodata': -9999,
                                  'transform': transform.from_bounds(
                                      win[0], win[1], win[2],
                                      win[3],
                                      valid.shape[1],
                                      valid.shape[0])
                                  }) as dst:
                for z in range(count):
                    dst.write_band(z+1, valid[:,:,z])



        else:
            print("NOT YET FINISHED")
            # aggregate = np.nanmedian(valid, axis=0)
            # if len(aggregate.shape)>2:
            #     aggregate_reshaped = aggregate[:,:,-5]#np.reshape(aggregate,
            #                                          # (aggregate.shape[-1],
            #                                     # aggregate.shape[0],
            #                                     # aggregate.shape[1]))
            #     count = 1#aggregate.shape[-1]
            # else:
            #     aggregate_reshaped=aggregate
            #     count=1
            # win_str = [str(x) for x in win]
            #
            # print(count)
            # print(aggregate_reshaped.shape)

        # aggregate = np.nanmedian(valid, axis=)
        #
        #     print(data.shape)
        # download(cloud_masks)


if __name__=='__main__':

    args = PARSER.parse_args()
    output_directory = make_output_dir(args)

    # bbox = get_bbox_from_wkt(args.wkt)
    # bbox = BBox([-16.12, 47.00,-16, 47.29], crs=CRS.WGS84)
    # bbox = BBox([14.419052876760505, -90.92164993286133, 14.55201636533031, -90.81865310668945], crs=CRS.WGS84)
    # window_list = get_windows(bbox,
    #                           max_window=512,
    #                           pixel_size=int(args.resolution))
    pixel_size, window_size = get_window_size(args.resample)

    window_list = wkt_to_gdf(args.wkt, window_size)


    logger = get_logger(output_directory)
    logger.info("Processing {} chunks".format(len(window_list)))
    for j,polygon in window_list.iterrows():
        win = polygon.geometry.bounds
        progress(j, len(window_list))
        bbox = BBox(win, crs=CRS.WGS84)
        bbox = convert_bbox_to_utm(bbox)
        cloud_masks = wcs_request(output_directory,
                          args.bands,
                          bbox,
                          args.start,
                          args.end,
                          pixel_size,
                          args.maxcloud,
                          args.cloudthresh
                          )
        # print(cloud_masks.get_dates())
        # print(cloud_masks.valid_data())
        if len(cloud_masks.get_dates())>0:
            write_raster(win, cloud_masks, output_directory, args)


