# NOTE: originally this image inherited from dprof. Now it inherits from
# Dockerfile-base to speed up CI significantly.

FROM registry.gitlab.com/satelligence/classifier:base

RUN apt-get update
RUN apt-get install -y libsm6
RUN apt-get install -y  libxext6


# Copy the application's requirements.txt and run pip to install all
# dependencies. Remove build artifact.
ADD requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt
RUN rm /app/requirements.txt

# Add application logic
ADD s2process/ /app/s2process

#ADD tests/ /app/tests

#ADD pylintrc /app/pylintrc

ENTRYPOINT ["python3", "./s2process/cli.py"]
